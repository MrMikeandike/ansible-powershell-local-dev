# Summary

This is a quick way to start working on powershell modules for ansible quickly. 
I don't like setting it up manually every time, so this just saves a bit of time rather than having to dig through the documentation


# Steps

1. clone https://github.com/ansible/ansible into some directory
2. set $ansibleRoot to the root of the repo you cloned
3. change the module to whatever you want by tweaking everything below "BEGIN MODULE"
4. change args sent to the module by changing ./args.json
5. run the script!
