# Set $ErrorActionPreference to what's set during Ansible execution
$ErrorActionPreference = "Stop"

# Put in the path to your clone of ansible's repo. Example...
# $ansibleRoot = "C:\data\git\ansible"
$ansibleRoot = ""

# Relative path to args.json
$PATH_TO_ARGS_JSON = ".\args.json"



$ansibleCSharp = join-path $ansibleRoot "lib\ansible\module_utils"
# Set the first argument as the path to a JSON file that contains the module args

$args = @(, $(resolve-path $PATH_TO_ARGS_JSON).Path)

# Or instead of an args file, set $complex_args to the pre-processed module args
<#
$complex_args = @{
    _ansible_check_mode = $false
    _ansible_diff = $false
    path = "C:\temp"
    state = "present"
}
#>

# Import any C# utils referenced with '#AnsibleRequires -CSharpUtil' or 'using Ansible.;
# The $_csharp_utils entries should be the context of the C# util files and not the path
Import-Module -Name "${ansibleCSharp}\powershell\Ansible.ModuleUtils.AddType.psm1"
$_csharp_utils = @(
    [System.IO.File]::ReadAllText("${ansibleCSharp}\csharp\Ansible.Basic.cs")
)
Add-CSharpType -References $_csharp_utils -IncludeDebugInfo

# Import any PowerShell modules referenced with '#Requires -Module`
#Import-Module -Name "${ansibleroot}\powershell\Ansible.ModuleUtils.Legacy.psm1"

#------------------------------------------------------------------------------#
#---------------------- BEGIN MODULE ------------------------------------------#

#!powershell

# Copyright: (c) 2015, Jon Hawkesworth (@jhawkesworth) <figs@unity.demon.co.uk>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

#AnsibleRequires -CSharpUtil Ansible.Basic

$spec = @{
    options = @{
        state=@{type='str'}
    }
    
    supports_check_mode = $true
}



$module = [Ansible.Basic.AnsibleModule]::Create($args, $spec)

[Ansible.Basic.AnsibleModule]::WriteLine.Invoke("nice job!!!")

$module.result.test = "test success!!"

$module.exitjson()
